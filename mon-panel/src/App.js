import React from 'react';


function Show(props) {
    if (!props.warn) {
        return (
            <div className="warning">
                Tu as cliqué sur le premier bouton !
            </div>
        )
    }
    return null

}

function Show2(props) {
    if (!props.warn) {

        return (
            <div className="warning">
                Tu as cliqué sur le troisième bouton !
            </div>
        )
    }

    return null
}


function Show3(props) {
    if (!props.warn) {
        return (
            <div className="warning">
                Tu as cliqué sur le troisième bouton !
            </div>
        )
    }

    return null

}


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Show: true,
            Show2: true,
            Show3: true,

        };
        this.handleClick = this.handleClick.bind(this);
        this.handleClick2 = this.handleClick2.bind(this);
        this.handleClick3 = this.handleClick3.bind(this);


    }


    render() {
        return (
            <div class="accordion" id="accordionExample">

                    <div className="card">
                        <button onClick={this.handleClick} class="btn  btn-info" type="button" data-toggle="collapse"
                                data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            {this.state.Show ? 'bouton 1' : 'x'}
                        </button>
                    </div>
                    <div>
                        <Show warn={this.state.Show}/>
                    </div>



                    <div className="card">
                        <button onClick={this.handleClick2} class="btn  btn-success" type="button"
                                data-toggle="collapse"
                                data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            {this.state.Show2 ? 'bouton 2 ' : 'x'}
                        </button>
                    </div>
                    <div>
                        <Show2 warn={this.state.Show2}/>
                    </div>



                    <div className="card">
                        <button onClick={this.handleClick3} class="btn  btn-warning" type="button"
                                data-toggle="collapse"
                                data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" >
                            {this.state.Show3 ? 'bouton 3' : 'x'}
                        </button>
                    </div>
                    <div class="card">
                        <Show3 warn={this.state.Show3} />
                    </div>

            </div>



        );

    }

    handleClick() {
        this.setState(prevState => ({
                Show: !prevState.Show
            }
        ));
    }

    handleClick2() {
        this.setState(prevState => ({
                Show2: !prevState.Show2
            }
        ));
    }

    handleClick3() {
        this.setState(prevState => ({
                Show3: !prevState.Show3
            }
        ));
    }

}


export default App;
